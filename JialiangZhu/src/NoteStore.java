import java.util.ArrayList;

public class NoteStore {
    //创建textList储存Note对象
    ArrayList<Note> textList = new ArrayList<Note>();

    //往list里存入TextAndImage对象
    public void storeNote(TextAndImageNote textAndImagenote){
        textList.add(textAndImagenote);
    }

    //往list里存入TextNote对象
    public void storeNote(TextNote textnote){
        textList.add(textnote);
    }

    //打印类组中TextNote类型
    public void getAllTextNote(){
        for(int i =0; i<textList.size(); i++){
            if (textList.get(i) instanceof TextNote){
                System.out.println(((TextNote) textList.get(i)).getText());
            }
        }
    }

    //打印类组中TextAndImageNote类型
    public void getAllTextAndImageNote(){
        for(int i =0; i<textList.size(); i++){
            if (textList.get(i) instanceof TextAndImageNote){
                System.out.println(((TextAndImageNote) textList.get(i)).getText()+" "+((TextAndImageNote) textList.get(i)).getImageUrl());
            }
        }
    }

}
