//子类TextNote继承Note
public class TextNote extends Note {

    private String text;

    public TextNote() {
    }

    public TextNote(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
