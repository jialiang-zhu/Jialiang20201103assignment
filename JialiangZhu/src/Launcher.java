public class Launcher {
    public static void main(String[] args) {

        //创建对象
        NoteStore noteStore = new NoteStore();
        TextNote note1 = new TextNote("Java is a set of Computer Software");
        TextNote note2 = new TextNote("Ikigai,How to win friends and influence people");
        TextAndImageNote iNote1 = new TextAndImageNote("the shopping list on my fridge","//foo/bar1/imageset1.jpg");
        TextAndImageNote iNote2 = new TextAndImageNote("the size label of Jack's shirt","//foo/bar1/imageset2.jpg");
        //储存创建的对象
        noteStore.storeNote(note1);
        noteStore.storeNote(note2);
        noteStore.storeNote(iNote1);
        noteStore.storeNote(iNote2);
        //调用display方法，在控制台在显示对象内的String
        displayTextNotes(noteStore);
        System.out.println();
        displayTextandImageNotes(noteStore);

    }


        //显示TextNote的静态方法
        public static void displayTextNotes(NoteStore noteStore){
           noteStore.getAllTextNote();
        }

        //显示TextandImageNote的静态方法
        public static void displayTextandImageNotes(NoteStore noteStore){
            noteStore.getAllTextAndImageNote();
        }



}

